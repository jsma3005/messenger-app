import React, { useEffect, useState } from 'react';
import {Container, Card, Form, InputGroup, Button} from 'react-bootstrap';
import './auth.css';
import {Link} from 'react-router-dom';
import {fire} from '../../services/firebase';

const Auth = props =>{
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [username, setUsername] = useState('');
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const isLogin = props.match.path === "/login";
    const pageTitle = isLogin ? "Sign In" : "Sign Up";
    const linkTitle = isLogin ? "Sign Up" : "Sign In";
    const linkDescription = isLogin ? "Don't have an account?" : "Already have an account?";
    const authLink = isLogin ? "/register" : "/login";

    const signIn = event =>{
        event.preventDefault();
        fire.auth().signInWithEmailAndPassword(email, password).then(res =>{
            setIsLoggedIn(true);
        }).catch(err =>{
            console.log(err);
            alert(err.message);
        })
    }

    const signUp = event =>{
        event.preventDefault();
        if(username !== ''){
            fire.auth().createUserWithEmailAndPassword(email, password).then(res =>{
                const user = res.user;
                user.updateProfile({
                    displayName: username
                })
                setEmail('');
                setPassword('');
                
                // Firestore
                fire.firestore().collection('users').add({
                    username,
                    email,
                    uid: res.user.uid
                })
                props.history.push('/login');
            })
            .catch(err =>{
                console.log("Error", err);
                alert(err.message)
            })
        }else{
            alert("Не все поля заполнены!");
        }
    }

    useEffect(() =>{
        if(isLoggedIn){
            props.history.push('/');
        }

        setEmail('');
        setPassword('');
    }, [props.history, isLoggedIn, isLogin]);

    return(
        <Container fluid className="auth_main">
            	<div className="auth_container">
                    <Card>
                        <Card.Header>
                            <h3>{pageTitle}</h3>
                            <div className="d-flex justify-content-end social_icon">
                                <span><i className="fab fa-facebook-square"></i></span>
                                <span><i className="fab fa-google-plus-square"></i></span>
                                <span><i className="fab fa-twitter-square"></i></span>
                            </div>
                        </Card.Header>
                        <Card.Body>
                            <Form onSubmit={isLogin ? signIn : signUp}>
                                {!isLogin && (
                                    <Form.Group className="input-group">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>
                                                <i className="fas fa-user"></i>
                                            </InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <Form.Control onChange={e =>{
                                            setUsername(e.target.value)
                                        }} type="text" placeholder="Username" />
                                    </Form.Group>
                                )}
                                <Form.Group className="input-group">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>
                                            <i className="fas fa-envelope"></i>
                                        </InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <Form.Control 
                                        type="email" 
                                        placeholder="Email"
                                        onChange={(e) =>{
                                            setEmail(e.target.value)
                                        }}
                                        value={email || ""}    
                                    />
                                </Form.Group>
                                <Form.Group className="input-group">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>
                                            <i className="fas fa-key"></i>
                                        </InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <Form.Control 
                                        type="password" 
                                        placeholder="Password" 
                                        onChange={(e) =>{
                                            setPassword(e.target.value)
                                        }}
                                        value={password || ""}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Button type="submit" value="Login" className="btn float-right login_btn">{pageTitle}</Button>
                                </Form.Group>
                            </Form>
                        </Card.Body>
                        <Card.Footer>
                            <div className="d-flex justify-content-center links">
                                {linkDescription}
                                <Link to={authLink}>{linkTitle}</Link>
                            </div>
                            <div className="d-flex justify-content-center">
                                <Link to="/forgor">Forgot your password?</Link>
                            </div>
                        </Card.Footer>
                    </Card>
                </div>
        </Container>
    )
}



export default Auth;