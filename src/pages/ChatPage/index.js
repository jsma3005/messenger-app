import React from 'react';
import Chat from '../../components/chat';
import Navigation from '../../components/navigation';
import SideBar from '../../components/sidebar';
// import { fire } from '../../services/firebase';

const ChatPage = (props) =>{
    
    return(
        <div className="container-fluid p-0">
            <Navigation />
            <div className="row m-0 pt-3">
                <div className="col-lg-2">
                    <SideBar />
                </div>
                <div className="col-lg-10">
                    <Chat props={props} />
                </div>
            </div>
        </div>
    )
}

export default ChatPage;