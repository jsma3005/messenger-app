import React from 'react';
import Navigation from '../../components/navigation';
import SideBar from '../../components/sidebar';

const HomePage = (props) =>{
    // const [user, setUser] = useState('');

    // useEffect(() =>{
    //     fire.auth().onAuthStateChanged(user =>{
    //         if(user){
    //             setUser(user.displayName);
    //         }
    //     })
    // }, [setUser])
    
    return(
        <div className="container-fluid p-0">
            <Navigation />
            <div className="row m-0 pt-3">
                <div className="col-lg-2">
                    <SideBar />
                </div>
                <div className="col-lg-10">
                    <h1>Добро пожаловать на мессенджер! Выберите собеседника, чтобы начать беседу!</h1>
                </div>
            </div>
        </div>
    )
}

export default HomePage;