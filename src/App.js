import React, { useState } from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import HomePage from './pages/Home';
import ChatPage from './pages/ChatPage';
import Auth from './pages/Auth';
import {fire} from './services/firebase';

function App() {

  const [user, setUser] = useState(() =>{
    fire.auth().onAuthStateChanged(res =>{
      setUser(res);
    })
  })

  return (
    <Switch>
      <Route exact path="/">
        {
          user === null ? <Redirect to="/login" /> : <HomePage />
        }
      </Route>
      <Route path="/user/:id" render={props =>(
        user === null ? <Redirect to="/login" /> : <ChatPage {...props} />
      )} />
      <Route path="/register" component={Auth} />
      <Route path="/login" component={Auth} />
      <Redirect to="/" />
    </Switch>
  );
}

export default App;
