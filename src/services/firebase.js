import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/firebase-firestore';

const firebaseConfig = {
    apiKey: "AIzaSyARKCFufuEYQAi6VDTQNBg_SoMkcfcAWnY",
    authDomain: "web-messenger-8fc8e.firebaseapp.com",
    databaseURL: "https://web-messenger-8fc8e.firebaseio.com",
    projectId: "web-messenger-8fc8e",
    storageBucket: "web-messenger-8fc8e.appspot.com",
    messagingSenderId: "392136807061",
    appId: "1:392136807061:web:b269e002c21acf0748c19c"
};

export const fire = firebase;
firebase.initializeApp(firebaseConfig);
