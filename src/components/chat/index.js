import React, { useEffect, useState } from 'react';
import { fire } from '../../services/firebase';
import LoadSpinner from '../loadSpinner';
import cls from './chat.module.css';
import Message from './message/Message';

const Chat = ({props}) =>{
    const [currentUser, setCurrentUser] = useState({});
    const [message, setMessage] = useState('');
    const [chat, setChat] = useState([]);

    const urlId = props.match.params.id;

    useEffect(() =>{
        fire.auth().onAuthStateChanged(user =>{
            setCurrentUser(user);
        })

        fire.database().ref(`/messages`).on('value', res =>{
            setChat(Object.values(res.val()).map(i => i))
        })

    }, [setCurrentUser, setChat, message])

    const sendMessage = (e) =>{
        e.preventDefault();

        // !!!!!!!!!!!!! Realtime database

        if(message !== ''){
            fire.database().ref(`/messages/${Date.now()}`).set({
                from: currentUser.uid,
                to: urlId,
                msg: message,
                time: new Date().toLocaleString()
            }).then(() =>{
                setMessage('');
            })
        }
    }

    const handleSetMessage = (e) =>{
        setMessage(e.target.value)
    }


    return(
        <div className={`card ${cls.card}`}>
            <div className={`card-body bg-light ${cls.cardBody}`}>
                {/* {chat.map(item => {
                    if((currentUser.uid === item.from && urlId === item.to) || (currentUser.uid === item.to && urlId === item.from) ){
                        return (
                            <Message mess={item.msg} className={item.to === urlId ? `${cls.right} alert-primary` : 'alert-danger'} key={item.time} id={urlId} />
                        )
                    }
                })} */}

                { chat.length !== 0 ? chat.filter(item => (currentUser.uid === item.from && urlId === item.to) || (currentUser.uid === item.to && urlId === item.from)).map(m => (
                    <Message mess={m.msg} className={m.to === urlId ? `${cls.right} alert-primary` : 'alert-danger'} key={m.time} id={urlId} date={m.time} />
                )) : <LoadSpinner />}
            </div>
            <div className="card-footer bg-primary">
                <form onSubmit={sendMessage}>
                    <div className="form-group">
                        <input className="form-control mb-3" placeholder="Введите сообщение..." value={message} onChange={handleSetMessage}></input>
                        <div className="text-center">
                            <button className="btn btn-danger w-100">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Chat