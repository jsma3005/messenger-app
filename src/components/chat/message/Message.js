import React from 'react';

const Message = (props) =>{

    const classes = {
        display: 'flex',
        flexDirection: 'column'
    }

    return (
        <p style={classes} className={`alert alert-primary ${props.className}`}>{props.mess} 
        <span style={{fontSize: '10px'}}>{props.date}</span> </p>
    )
}

export default Message;