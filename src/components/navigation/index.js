import React from 'react';
import {Link} from 'react-router-dom';
import logo from './img/chat.png';
import cls from './sidebar.module.scss';
import {fire} from '../../services/firebase';

const Navigation = () =>{
    const signOut = () =>{
        fire.auth().signOut();
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <Link className="navbar-brand" to="/">
                <img className={cls.logo} src={logo} alt="" />
            </Link>
            <ul className="navbar-nav ml-auto">
                <li className="nav-link">
                    <Link className="btn btn-light" to="/user-settings">
                        <i className="fas fa-user-cog"></i>
                    </Link>
                </li>
                <li className="nav-link">
                    <button onClick={signOut} className="btn btn-light">
                        <i className="fas fa-sign-out-alt"></i>
                    </button>
                </li>
            </ul>
        </nav>
    )
}

export default Navigation;