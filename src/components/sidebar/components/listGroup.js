import React from 'react';
import {NavLink} from 'react-router-dom';
import cls from './listGroup.module.css';

const ListItem = ({name, uid}) =>{
    return (
        <NavLink to={`/user/${uid}`} className="list-group-item list-group-item-action">
            <img className={cls.img} src="https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png" alt="" />
            {name}
        </NavLink>
    )
}

export default ListItem;