import React, { useEffect, useState } from 'react';
import ListItem from './components/listGroup';
import cls from './sidebar.module.css';
import {fire} from '../../services/firebase';
import LoadSpinner from '../loadSpinner';

const SideBar = () =>{

    const [users, setUsers] = useState([]);
    const [currentUser, setCurrentUser] = useState({});
    
    useEffect(() =>{
        // Firestore
        fire.firestore().collection('users').get().then(res =>{
            let usersData = [];
            res.forEach(user =>{
                usersData.push(user.data());
            })
            setUsers(usersData);
        })

        fire.auth().onAuthStateChanged(user =>{
            setCurrentUser(user);
        })

    }, [setUsers, setCurrentUser]);

    return (
        <div className={`${cls.list} list-group`}>
            { users.length === 0 ? <LoadSpinner /> : 
                users.map(item => (
                    item.uid !== currentUser.uid && ( 
                        <ListItem key={item.uid} name={item.username} uid={item.uid} />
                    )
                ))
            }
            {/* {users.map(item => (
                item.uid !== currentUser.uid && ( 
                    <ListItem key={item.uid} name={item.username} uid={item.uid} />
                )
            ))} */}
        </div>
    )
}

export default SideBar;